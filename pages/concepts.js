import Head from 'next/head'
import conceptsContent from '../content/conceptsContent'
import Sidebar  from './modules/sidebar'

import { useContext } from 'react';
import LanguageSelect from '../lib/languageSelect'


export default function concepts() {
  console.log(conceptsContent.english)
 
  const lang = useContext(LanguageSelect)
  const path = conceptsContent.english.concepts
    const renderSidebar = path.map(({ id }) => {
      const target = id
      const label = id
      return {
        target,
        label
      }
  });


  const renderContent = () => {
    return path.map(({ title, content, }, index) => (
      <div key={index}>
        <h4>{title}</h4>
          <div>
            <p>{content}</p>
          </div>
      </div>
    ));
  };

    return (
      <div className="container">
        <Head>
          <title>Xalgorithms Foundation</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
          <main>
            <div className="sticky">
                <Sidebar 
                  options={renderSidebar}
                />
            </div>
            <div className="singlecol">
              <h1>
                  concepts
              </h1>
              {renderContent()}
            </div>
          </main>
      </div>
    )
  }