import 'xf-material-components/package/config/rootStyle.css'
import '../styles/global.css'
import NavBar from '../components/navBar'
import { useState } from 'react';
import LanguageSelect from '../lib/languageSelect'
import Footer from '../components/Footer'

export default function App({ Component, pageProps }) {
  const [lang, setLang] = useState("english");

  function handleLanguageChange(e){
    setLang(e.target.value);
  };

  return (
  <div className="hold">
      <NavBar
        select={handleLanguageChange}
        setLang={lang}
        options={[
          { value: 'english', label: 'English' },
          /*{ value: 'spanish', label: 'Spanish' },
          { value: 'france', label: 'French' },*/
        ]}
      />
      <div id="mainhold">
        
        <LanguageSelect.Provider value={lang}>
          <Component {...pageProps} />
        </LanguageSelect.Provider>
      </div>
      <Footer />
  </div>
  );
}
