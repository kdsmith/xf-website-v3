import Link from 'next/link'



export default function Sidebar({ options = [] }) {
  const renderOptions = () => {
    return options.map(({ target, label, }, index) => (
      <Link href={'/uses#'+target} key={index}>
        {label}
      </Link>
    ));
  };
    return (
      <div id="sidenav">
        {renderOptions()}
      </div>
    )
  }