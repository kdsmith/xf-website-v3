import AboutAuthor from '../../components/AboutAuthor'
import Tags from '../../components/Tags'
import Head from 'next/head'
import { getAllPostIds, getPostData } from '../../lib/posts'
import { useEffect, useState } from 'react'
import { Breadcrumbs } from '../../components/Bcrumb'
import { InputText } from 'xf-material-components/package/index'
import renderToString from 'next-mdx-remote/render-to-string';
import hydrate from 'next-mdx-remote/hydrate';
import { Button } from 'xf-material-components/package/index'
import Logo from '../../components/Logo'

const components = { Button, Logo }


export async function getStaticPaths() {
    const paths = getAllPostIds()
    return {
      paths,
      fallback: false
    }
  }

  export async function getStaticProps({ params }) {
    const postData = await getPostData(params.id) 
    const mdxSource = await renderToString(postData.mdxPath, { components })
    return {
      props: {
        postData,
        source: mdxSource,
      }
    }
  }

  const widtht = {
    display: 'block'
  }

  

  

  export default function Post({ postData, source }) {
    const [scrollTop, setScrollTop] = useState(0);
    const [bgColor, setBgColor] = useState('var(--firmament)')

    const bcrumb = Breadcrumbs()

    const content = hydrate(source, { components });

    useEffect(() => {
      
      if (postData.tags[0].tag === "era") {
        setBgColor("var(--lichen)")
      } else if (postData.tags[0].tag === "xrm dev") {
        setBgColor("var(--satin)")
      }
      
    }, [scrollTop]);

    

    const bgimg = {
      backgroundImage: "url(/" + postData.featimg + ")",
      width: "100%",
      height: "600px",
      backgroundSize: "cover",
      backgroundPosition: "center center",
      backgroundBlendMode: "color-burn",
      backgroundColor: bgColor
    }

    return (
      <div className="container">
        <Head>
          <title>{postData.title}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <main>
          <div style={widtht}>
            <div className="grid12">
              <div className="imghold">
                <div style={bgimg}/>
              </div>
            </div>
          </div>
          <div className="singlecol">
            <div style={widtht}>
              <div className="WritingGrid12" id="writingTopMargin">
                <div className="writingGridHold">
                  <h2>{postData.title}</h2>
                    <div className="textm">
                          {content}
                    </div>
                  <div className="ctaHold">
                    <h5>Want to Read More?</h5>
                    <br/>
                    <p className="smallT">
                      Join the email list and we'll send you updates on the Xalgorithms Foundation, our projects, writings, and ideas. 
                    </p>
                    <br/>
                    <InputText placeholder="Sign up for updates"/>
                  </div>
                </div>
                <div className="sticky">
                  <div className="appearance">
                    <div>{bcrumb}</div>
                    <Tags tags={postData.tags} />
                  </div>
                  <AboutAuthor image={postData.authorProfile} name={postData.author} description={postData.authorDescription} version={postData.version} date={postData.date}/>
                </div>
              <div />
            </div>
            </div>
          </div>
        </main>
      </div>
    )
  }

  