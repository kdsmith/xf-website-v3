import Head from 'next/head'
import AboutAuthor from '../components/AboutAuthor'
import communityContent from '../content/communityContent'
import Header from '../components/Header'

import { useContext } from 'react';
import LanguageSelect from '../lib/languageSelect'

export default function community() {
  const lang = useContext(LanguageSelect)
  const content = communityContent[lang].team

  const topGrid = {
    gridArea : "1 / 2 / 1 / 6"
  }

  const sideGrid = {
    gridArea : "1 / 8 / 1 / 11",
    alignSelf: "center"
  }

  const img = {
    width: '100%'
  }

  const grida = {
    gridArea : "1 / 2 / 1 / 4"
  }

  const gridb = {
    gridArea : "1 / 5 / 1 / 6"
  }

  const gridc = {
    gridArea : "1 / 8 / 1 / 9"
  }

  const renderTeam = () => {
    return content.map(({img, name, bio, links}, index ) => (
      <div className="communityProfileHold" key={index}>
        <AboutAuthor image={img} name={name} description={bio} links={links}/>
      </div>
    ));
  }

    return (
      <div className="container">
        <Head>
          <title>Xalgorithms Foundation</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <main>
        <Header 
          title="Community Headline."
          description="Sentence about getting involved."
          buttonLable="A good CTA"
          target="https://gitlab.com/xalgorithms-alliance/xf-material-components"
        />
          <div className="grid12">
            <div className="communityTopGrid">
              <img src="distribute.gif" style={img}/>
            </div>
            <div className="communitySideGrid">
              <p>
                In the 1990s, Bill and Joseph independently and unknown to each other, conceptualized, planned, and developed elements of the 'oughtomation' method. In the following 25 years, Bill implemented his ideas in the private sector on mainframe systems for large financial institutions. Joseph implemented his ideas in the public sector, in part while leading an early free/libre user- controlled cloudlet system for high-performance distributed computing. Upon first meeting in late 2014, the two found their respective concepts so obviously synergistic that within a week they began dedicated collaboration on 100% free/libre and open source terms. Don Kelly brought four years of technical virtuosity and distributed system creativity, for a general purpose solution. 
              </p>
            </div>
            
          </div>
          <div className="communityGridHold">
            {renderTeam()}
          </div>
        </main>
      </div>
    )
  }