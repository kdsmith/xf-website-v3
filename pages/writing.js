// import { redirect } from 'next/dist/next-server/server/api-utils'
import Link from 'next/link'
import { getSortedPostsData } from '../lib/posts'
import Tag from '../components/Tag'
import SignUp from '../components/SignUp'


export async function getStaticProps() {
    const allPostsData = getSortedPostsData()
    return {
      props: {
        allPostsData
      }
    }
  }

export default function writing ({ allPostsData }) {
  const renderPost = () => {
    return(allPostsData.map(({ id, title, published, tags=[] }) => (
      published ? ( 
        <li key={id}>
            {tags.map(({ tag }, index) => (
              <Tag target={tag} key={index} />
            ))}
            <p className="featuredtitle">
              {title}
            </p>
            <Link href={'/writing/'+id}>
                <a className="noUnderline">
                  Read   →
                </a>
            </Link>
        </li>
      ) : ( 
        null
      )
      
    )))
  }
    return (
     
        <main>
          <section>
            <div className="WritingGrid12" id="bottomMargin">
              <div className="sideCTA" >
                <SignUp />
              </div>
              <div className="animationTwo" className="seven-eight" >
                <ul className="writingHold">
                {renderPost()}
                </ul>
              </div>
            </div>
          </section>
        </main>
      
    )
  }