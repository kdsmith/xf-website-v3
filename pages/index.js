import React, {  useEffect, useRef, useState } from 'react';
import { Button } from 'xf-material-components/package/index'
import Link from 'next/link'
import { getSortedPostsData } from '../lib/posts'
import Tag from '../components/Tag'
import SignUp from '../components/SignUp'
import { useContext } from 'react';
import LanguageSelect from '../lib/languageSelect'
import content from '../content/content'

export async function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData
    }
  }
}

export default function Home({ allPostsData }) {

  // gets content for the selected language
  const lang = useContext(LanguageSelect)
  const copy = content[lang]

  const mainhold = {
    display: 'flex',
    alignItems: 'flex-end',
    minHeight: '60vh',
    paddingTop: '81px',
    justifyContent: 'space-between'
  }

  const writingTimeHold = {
    display: 'grid',
    width: '100vw',
    gridTemplateColumns: '1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr',
    alignItems: 'center',
    marginBottom: '4em'
  }

  const marginal = {
    marginTop: '0.5em',
    marginBottom: '0.5em'
  }

  const ahold = {
    gridArea: '1 / 2 / 2 / 6',
  }

  const imghold = {
    overflow: 'hidden',
    gridArea: '1 / 7 / 2 / 12',
    width: '100%',
    height: '400px'
  }

  const present = {
    width: '100%'
  }

  const writingH = {
    display: 'grid',
    width: '100%',
    gridTemplateColumns: '1fr 1fr 1fr'
  }

  const width = {
    gridArea: '1 / 2 / 2 / 12',
  }

  const middleline = {
    width: '200px',
    margin: 'auto',
    textAlign: 'center',
    marginBottom: '4em'
  }
  const paddingunit = {
    height: '20vh'
  }

  // p5.js interactive graphic
  const ref = useRef(null);

  const [sketchWidth, setSketchWidth] = useState(0)
  const [sketchHeight, setSketchHeight] = useState(0)

  const renderGraphic = () => {
    useEffect(() => {
      const Sketch = require("p5")
      const s = ( Sketch ) => {
      
      setSketchWidth(ref.current ? ref.current.offsetWidth : 0)
      setSketchHeight(ref.current ? ref.current.offsetWidth : 0)
      const w = sketchWidth,
            h = sketchHeight,
            r = sketchWidth/2,
            xa = 0-(sketchWidth/8),
            xb = sketchWidth+(sketchWidth/8),
            ya = 0-(sketchHeight/8),
            yx = sketchHeight+(sketchHeight/8);


        Sketch.preload = () => {
          Sketch.img0 = Sketch.loadImage('./feata.png')
          Sketch.img1 = Sketch.loadImage('./featb.png')
          Sketch.img2 = Sketch.loadImage('./featc.png')
          Sketch.img3 = Sketch.loadImage('./featd.png')
        }

        Sketch.setup = () => {
          Sketch.createCanvas(w, h);
          Sketch.noLoop();
        };

        

        Sketch.draw = () => {
          Sketch.background(241, 238, 252);
          Sketch.blendMode(Sketch.BURN);
          Sketch.imageMode(Sketch.CENTER);
          Sketch.image(Sketch.img0, Sketch.random(xa, xb), Sketch.random(-80, 600), r, r);
          Sketch.image(Sketch.img1, Sketch.random(xa, xb), Sketch.random(-80, 600), r, r);
          Sketch.image(Sketch.img3, Sketch.random(xa, xb), Sketch.random(-80, 600), r, r);
          Sketch.image(Sketch.img2, Sketch.random(xa, xb), Sketch.random(-80, 600), r, r);
     
        }
    

      }
      let myp5 = new Sketch(s, 'featuredGraphic');
    }, [ref.current])
  }
  renderGraphic()
  
  
  // retrieve featured posts

  const renderPost = () => {
    return(allPostsData.map(({ id, title, featured, tags=[] }, index) => (
      featured ? ( 
        <div key={index}>
            {tags.map(({ tag }, index) => (
              <Tag target={tag} key={index} />
            ))}
            <p className="featuredtitle">
              {title}
            </p>
            <Link href={'/posts/'+id}>
                <a className="noUnderline">
                  Read   →
                </a>
            </Link>
        </div>
      ) : ( 
        null
      )
      
    )))
  }
  

  return (
    <>
    <div className="indexMainHold">
      <div className="indexHeadlineHold">
        <div className="indexAHold">
        <h3 className="build">{copy.landing.hook}</h3>
        <br />
        <br />
        <div className="animationTwo">
          <Button>{copy.landing.cta}</Button>
        </div>
        <br />
        <div className="contentHold">
          <div>
            <br />
              <p>
                {copy.landing.problem}
              </p>
            <br />
            <a className="noUnderline">
              {copy.landing.problemCta}
            </a>
          </div>
          <div />
          <div>
            <br />
            <p>
              {copy.landing.solution}
            </p>
            <br />
            <a className="noUnderline">
              {copy.landing.solutionCta}
            </a>
          </div>
        </div>
        <br />
        
      </div>
      <div className="indexImgHold" ref={ref}>
          <div id="featuredGraphic">
            <div id="p5_loading" className="loadingclass"></div>
          </div>
        </div>
      </div>
    </div>
    <div className="indexWritingTimeHold" id="animationThree">
      <div style={width}>
            <div className="indexWritingHold">
              {renderPost()}
            </div>
      </div>
    </div>
    <div style={writingTimeHold}>
      
      <div style={width}>
      <div style={paddingunit}/>
        <div style={middleline}>
          <SignUp />
        </div>
        </div>
    </div>
    </>
          
  )
}
