import style from './AboutAuthor.module.css'

export default function AboutAuthor({ image, name, description, version, date, links=[] }) {
    return (
        <div className="linkHold">
            <div className={style.authorFlex}>
                <div className={style.profileHold}>
                    <img src={image} className={style.imgsize}/>
                </div>
                <div>
                <div className={style.versionFlex}>
                    <p className={style.sansSerif}>
                        {'v '+version}
                    </p>
                    <p className={style.sansSerif}>
                        {date}
                    </p>
                </div>
                <h5>{name}</h5>
                </div>
            </div>
            <p className={style.sansSerif}>{description}</p>
            { links[0] ? (
            links.map(({name, target}, index) => (
                <a key={index} href={target}>{name}</a>
             ))
            ) : (
                null
            )
            }
        </div>
    )
}