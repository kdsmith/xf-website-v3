import Link from 'next/link'
import styles from './CTAlarge.module.css'

export default function CTALarge({ title, id, cta, desc }) {
    return (
        <Link href={'../posts/'+id}>
            <div className={styles.CTALarge}>
                <div  className={styles.top}>
                    <h4>{title}</h4>
                    <div className={styles.ctaflex}>
                        <p className={styles.cta}>{cta}</p>
                        <svg width="15" height="15.37" viewBox="0 0 15 15.37">
                            <defs>
                                <rect y="0.49" width="14" height="14.38" fill="none"/>
                            </defs>
                            <title>external</title>
                            <g clipPath="url(#a)">
                                <path d="M13.9,15.37H3.75a.5.5,0,0,1-.5-.5.5.5,0,0,1,.5-.5H13.5V1H.5v11.8l8.29-8.3h-5a.5.5,0,0,1-.5-.5.5.5,0,0,1,.5-.5H10a.58.58,0,0,1,.19,0,.4.4,0,0,1,.15.1h0l0,0h0a.6.6,0,0,1,.09.15.41.41,0,0,1,0,.19v6.69a.5.5,0,0,1-1,0V5.2l-9,9a.58.58,0,0,1-.65.12.6.6,0,0,1-.37-.55V.59A.6.6,0,0,1,.1,0H13.9a.6.6,0,0,1,.6.6V14.77A.6.6,0,0,1,13.9,15.37Z" transform="translate(0.5 0.01)" fill="#fff"/>
                            </g>
                        </svg>
                    </div>
                </div>
                <div className={styles.descriptionhold}>
                    <p className={styles.cta}>{desc}</p>
                </div>
            </div>
        </Link>
    )
  }