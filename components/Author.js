//import Link from 'next/link'
import style from './Author.module.css'

export default function Author({ title, version, date, tags=[] }) {

    const renderTags = () => {
        return tags.map(({ tag }, index) => (
          <a href={tag} key={index} className={style.blue} className={style.tagHold}>
            {tag}
          </a>
        ));
      };

    return (
        <div>
            <div className={style.dateFlex}>
                <div>
                    {renderTags()}
                </div>
                <div className={style.versionFlex}>
                    <p className={style.sansSerif}>
                        {'v '+version}
                    </p>
                    <p className={style.sansSerif}>
                        {date}
                    </p>
                </div>
            </div>
            <h1>
                {title}
            </h1>
        </div>
    )
  }
