import style from './Footer.module.css'

export default function Footer() {
    return (
        <>
            <div className={style.footerhold}>
                <div>
                    <p>
                    Text Graphics CC-by International 4.0  
                    <br />
                    Core Software Apache 2.0 
                    <br />
                    Extensions Affero GPL 3.0
                    </p>
                </div>
                <div>
                    <a className={style.footerlink}>
                        XRM dev
                    </a>
                </div>
                <div>
                    <a className={style.footerlink}>
                        Developer Docs
                    </a>
                    <a className={style.footerlink}>
                        Source Code
                    </a>
                    <a className={style.footerlink}>
                        Design and Brand
                    </a>
                </div>
                <div>
                    <a className={style.footerlink}>
                        Tritter
                    </a>
                    <a className={style.footerlink}> 
                        LinkedIn
                    </a>
                    <a className={style.footerlink}>
                        Email
                    </a>
                </div>
                <div>
                    <a className={style.footerlink}>
                        Donate
                    </a>
                    <a className={style.footerlink}>
                        Archive
                    </a>
                </div>
            </div>
        </>
    )
}