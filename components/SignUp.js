import { Button } from 'xf-material-components/package/index'

export default function SignUp() {
    return ( 
    <>
        <h5>
            We're Building
        </h5>
        <br />
        <p>
            Join the Xalgorithms Foundation email list to keep up do date with our progress.
        </p>
        <br />
        <Button>
            Sign up
        </Button>
    </>
    )
}