import React, { useRef, useEffect } from 'react'

export default function Tag ({target}) {
    

    const era = {
        background: 'var(--seafoam)',
        color: '#3C6444',
        padding: '0.2em',
        borderRadius: '0.2em',
        fontSize: '0.65em',
        textTransform: 'uppercase',
        textDecoration: 'none',
        marginRight: '1.6em',
        boxShadow: '0 0 4px 4px #E4F2E7'
    }

    const xrmDev = {
        background: 'var(--clear)',
        color: 'var(--naval)',
        padding: '0.2em',
        borderRadius: '0.2em',
        fontSize: '0.65em',
        textTransform: 'uppercase',
        textDecoration: 'none',
        marginRight: '1.6em',
        boxShadow: '0 0 4px 4px var(--clear)'
    }

    const standard ={
        background: 'var(--satin)',
        color: 'var(--royal)',
        padding: '0.2em',
        borderRadius: '0.2em',
        fontSize: '0.65em',
        textTransform: 'uppercase',
        textDecoration: 'none',
        marginRight: '1.6em',
        boxShadow: '0 0 4px 4px var(--satin)'
    }

    const [tagType, setTagType] = React.useState(standard)

    function pickStyle (ref) {
        useEffect(() => {
            if (target === 'era') {
                setTagType(era)
            } else if (target === 'xrm-dev') {
                setTagType(xrmDev)
            } else {
                setTagType(standard)
            }
        }, [ref])
    }

    const wrapperRef = useRef(null)
    pickStyle(wrapperRef)

    return (
        <span ref={wrapperRef} style={tagType} >
            {target}
        </span>
    )
}