import Link from 'next/link'
import styles from './featuredwriting.module.css'

export default function FeaturedWriting({ img, imgalt, title, id, preview }) {
    return (
      <div>
        <img src={img} alt={imgalt}/>
        <h4>{title}</h4>
        <p>{preview}</p>
        <Link href={'../posts/'+id}>
          <a className={styles.blue}>
            read more
          </a>
        </Link>
      </div>
    )
  }