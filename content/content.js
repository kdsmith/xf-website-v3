   const Content =  {english: {
        landing: {
          hook: "Practical Normative Data.",
          cta: "An Invitation to Builders ",
          problem: "How can you discover and obtain facts about the normative constraints that are in effect and applicable to any undertaking at any given moment? ",
          problemCta: "Read the White Paper  →",
          solution: "Until now the world has lacked a common efficient way to communicate compulsion, option or expectation from rulemakers to rule-takers.",
          solutionCta: "Try XRM dev  →"
        }
    }
  }

  export default Content