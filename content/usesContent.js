 const usesContent = {
    "english": {
        uses: [
          {
            id: 'set', 
            title: 'Set Oughtomation', 
            content: 'Mauris bibendum pretium ultrices. Sed sed tortor iaculis nulla accumsan cursus. Donec interdum magna eu ultrices pellentesque. Aenean imperdiet sem ligula, eu pharetra mi maximus a. Maecenas id ipsum nunc. Donec felis tortor, maximus ut nibh eget, auctor pellentesque lectus. Mauris bibendum pretium ultrices. Sed sed tortor iaculis nulla accumsan cursus.',
            links: [
              {target: 'test', label: 'Sample Link'},
              {target: 'test', label: 'Sample Link'}
            ]
          },
          {id: 'price', title: 'Price Oughtomation', content: 'Mauris bibendum pretium ultrices. Sed sed tortor iaculis nulla accumsan cursus. Donec interdum magna eu ultrices pellentesque. Aenean imperdiet sem ligula, eu pharetra mi maximus a. Maecenas id ipsum nunc. Donec felis tortor, maximus ut nibh eget, auctor pellentesque lectus. Mauris bibendum pretium ultrices. Sed sed tortor iaculis nulla accumsan cursus.' },
          {id: 'role', title: 'Role Oughtomation', content: 'Mauris bibendum pretium ultrices. Sed sed tortor iaculis nulla accumsan cursus. Donec interdum magna eu ultrices pellentesque. Aenean imperdiet sem ligula, eu pharetra mi maximus a. Maecenas id ipsum nunc. Donec felis tortor, maximus ut nibh eget, auctor pellentesque lectus. Mauris bibendum pretium ultrices. Sed sed tortor iaculis nulla accumsan cursus.' },
          {id: 'machine', title: 'Machine Oughtomation', content: 'Mauris bibendum pretium ultrices. Sed sed tortor iaculis nulla accumsan cursus. Donec interdum magna eu ultrices pellentesque. Aenean imperdiet sem ligula, eu pharetra mi maximus a. Maecenas id ipsum nunc. Donec felis tortor, maximus ut nibh eget, auctor pellentesque lectus. Mauris bibendum pretium ultrices. Sed sed tortor iaculis nulla accumsan cursus.' }
        ]
    }
}

export default usesContent